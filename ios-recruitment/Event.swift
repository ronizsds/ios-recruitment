//
//  Event.swift
//  ios-recruitment
//
//  Created by Roniel Soares De Sousa on 21/04/17.
//  Copyright © 2017 Roniel Soares De Sousa. All rights reserved.
//

import Foundation
import UIKit

struct Event {
    let id: String
    let name: String
    let url: URL
    let logoURL: URL?
    let startDate: Date
    let eventType: String
    let location: Location
}

// MARK: Event Initializer

/// Add an optional initializer from JSON to Event.
extension Event {
    
    /// Returns nil if deserialization fails
    init?(json: [String: Any]) {
        
        // Try to extract all values
        guard let id = json["id"] as? String,
            let name = json["name"] as? String,
            let urlString = json["url"] as? String,
            let startDateString = json["start_date"] as? String,
            let eventType = json["event_type"] as? String,
            let locationJSON = json["location"] as? [String: Any]
        else {
                return nil
        }
        
        // Try to convert url strings to URL
        guard let url = URL(string: urlString) else {
            return nil
        }
        
        // Formatter to startDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        // Try to convert startDateString to URL
        guard let startDate = dateFormatter.date(from: startDateString) else {
            return nil
        }
        
        // Try to initialize Location from JSON
        guard let location = Location(json: locationJSON) else {
            return nil
        }
        
        // Initialize the optional logoURL
        if let logoURLString = json["logo_url"] as? String {
            self.logoURL = URL(string: logoURLString)
        } else {
            self.logoURL = nil
        }
        
        // Initialize properties
        self.id = id
        self.name = name
        self.url = url
        self.startDate = startDate
        self.eventType = eventType
        self.location = location
    }
}

// MARK: Network layer

/// Add networking to Event.
///
/// For simplicity, the network layer is inserted into the model.
///
/// In more complex scenarios, this layer would be created with
/// the help of some other library, such as Moya.
extension Event {
    
    // Base URL to make the requests
    private static let baseURL = URL(string: "https://s3.amazonaws.com/sympla-ios-recruitment/endpoints")!
    
    
    /// Helper function to request events from baseURL with path components.
    ///
    /// If success, call the callback with the results,
    /// if fails, call the callback with an empty vector.
    private static func requestEvents(pathComponents components: String = "", completion: @escaping ([Event]) -> Void) {
        let url = baseURL.appendingPathComponent(components, isDirectory: false)
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // If the request or deserialization fails, call completion callback with an empty vector
            guard let data = data, error == nil,
                let json = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any],
                let results = json["data"] as? [[String: Any]]
            else {
                completion([])
                return
            }
            
            var events: [Event] = []
            
            // Iterate over results and initialize the events from JSON
            for case let result in results {
                if let event = Event(json: result) {
                    events.append(event)
                }
            }
            
            // Call the callback with the results
            completion(events)
            
        }.resume()
    }
    
    /// Get the list of featured events.
    ///
    /// If success, call the callback with the results,
    /// if fails, call the callback with an empty vector.
    static func featuredEvents(completion: @escaping ([Event]) -> Void) {
        requestEvents(pathComponents: "featured_events.json", completion: completion)
    }
    
    /// Get the list of highlighted events.
    ///
    /// If success, call the callback with the results,
    /// if fails, call the callback with an empty vector.
    static func highlightedEvents(completion: @escaping ([Event]) -> Void) {
        requestEvents(pathComponents: "highlighted_events.json", completion: completion)
    }
}

/// Defines an extension to create a default image in event.
extension Event {
    static let defaultImage = UIImage(named: "event_placeholder")!
}




