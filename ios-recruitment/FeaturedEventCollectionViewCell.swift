//
//  FeaturedEventCollectionViewCell.swift
//  ios-recruitment
//
//  Created by Roniel Soares De Sousa on 21/04/17.
//  Copyright © 2017 Roniel Soares De Sousa. All rights reserved.
//

import UIKit

class FeaturedEventCollectionViewCell: UICollectionViewCell {
    
    var event: Event?
    
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    
    /// Configure the cell with given event.
    ///
    /// The index is needed because the image is download asynchronous.
    func configure(withEvent event: Event, index: Int) {
        
        self.event = event
        
        // First, configure the eventImageView
        // Set the default image and then download the image asynchronously
        self.eventImageView.image = Event.defaultImage
        
        // Check if url exists
        guard let url = event.logoURL else {
            // If fails, set default image
            self.eventImageView.image = Event.defaultImage
            return
        }
        
        self.tag = index
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
            // Download the image data
            let data = try? Data(contentsOf: url)
            
            // Try to initialize a UIImage from data
            guard let imageData = data, let imageFromData = UIImage(data: imageData) else {
                return
            }
            
            // Set the imageView in main queue
            DispatchQueue.main.async { [weak self] in
                // Only set if the tag has not changed
                if self?.tag == index {
                    self?.eventImageView.image = imageFromData
                }
            }
        }
    }
}
