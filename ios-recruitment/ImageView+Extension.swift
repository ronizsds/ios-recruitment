//
//  ImageView+Extension.swift
//  ios-recruitment
//
//  Created by Roniel Soares De Sousa on 21/04/17.
//  Copyright © 2017 Roniel Soares De Sousa. All rights reserved.
//

import UIKit

/// Add ability to download image asynchronously.
extension UIImageView {
    
    /// If fails, set an optional default image.
    public func imageFromURL(url: URL, defaultImage: UIImage? = nil) {
        
        // Set the default image
        self.image = defaultImage
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
            // Download the image data
            let data = try? Data(contentsOf: url)
            
            // Try to initialize a UIImage from data
            guard let imageData = data, let imageFromData = UIImage(data: imageData) else {
                return
            }
            
            // Set the imageView in main queue
            DispatchQueue.main.async { [weak self] in
                self?.image = imageFromData
            }
        }
    }
}
