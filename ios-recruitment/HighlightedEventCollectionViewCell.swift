//
//  HighlightedEventCollectionViewCell.swift
//  ios-recruitment
//
//  Created by Roniel Soares De Sousa on 21/04/17.
//  Copyright © 2017 Roniel Soares De Sousa. All rights reserved.
//

import UIKit

class HighlightedEventCollectionViewCell: UICollectionViewCell {
    
    var event: Event?
    
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    /// Configure the cell with given event.
    ///
    /// The index is needed because the image is download asynchronous.
    ///
    /// We should use cache to improve performance and get a better look in scrolling.
    func configure(withEvent event: Event, index: Int) {
        
        self.event = event
        
        // First, configure the eventImageView.
        // Set the default image and then download the image asynchronously.
        self.eventImageView.image = Event.defaultImage
        
        // Check if url exists
        guard let url = event.logoURL else {
            // If fails, set default image
            self.eventImageView.image = Event.defaultImage
            return
        }
        
        self.tag = index
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
            // Download the image data
            let data = try? Data(contentsOf: url)
            
            // Try to initialize a UIImage from data
            guard let imageData = data, let imageFromData = UIImage(data: imageData) else {
                return
            }
            
            // Set the imageView in main queue
            DispatchQueue.main.async { [weak self] in
                // Only set if the tag has not changed
                if self?.tag == index {
                    self?.eventImageView.image = imageFromData
                }
            }
        }
        
        // Configure all other views
        self.nameLabel.text = event.name
        self.locationLabel.text = event.location.name
        self.monthLabel.text = Calendar.current.shortMonthSymbols[Calendar.current.component(.month, from: event.startDate) - 1]
        self.dayLabel.text = String(format: "%02d", Calendar.current.component(.day, from: event.startDate))
        self.timeLabel.text = String(format: "%02dh", Calendar.current.component(.hour, from: event.startDate))
        self.cityLabel.text = "\(event.location.city), \(event.location.state)"
        
    }
}
