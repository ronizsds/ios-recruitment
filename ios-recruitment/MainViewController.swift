//
//  MainViewController.swift
//  ios-recruitment
//
//  Created by Roniel Soares De Sousa on 21/04/17.
//  Copyright © 2017 Roniel Soares De Sousa. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    // Properties
    
    var featuredEvents: [Event] = [] {
        didSet {
            DispatchQueue.main.async {
                self.featuredEventsCollectionView?.reloadData()
            }
        }
    }
    
    var highlightedEvents: [Event] = [] {
        didSet {
            DispatchQueue.main.async {
                self.highlightedEventsCollectionView?.reloadData()
            }
        }
    }
    
    
    // MARK: IBOutlets
    
    @IBOutlet weak var welcomeMessageLabel: UILabel!
    @IBOutlet weak var featuredEventsCollectionView: UICollectionView!
    @IBOutlet weak var highlightedEventsCollectionView: UICollectionView!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        featuredEventsCollectionView.dataSource = self
        featuredEventsCollectionView.delegate = self
        
        highlightedEventsCollectionView.dataSource = self
        highlightedEventsCollectionView.delegate = self
        
        Event.featuredEvents { self.featuredEvents = $0 }
        Event.highlightedEvents { self.highlightedEvents = $0 }
        
        configureWelcomeMessage()
    }
    
    // MARK: Functions
    
    func configureWelcomeMessage() {
        
        // This number must be gotten from the server.
        let numberOfEvents = 5150
        
        // Initial message
        let message = "\(numberOfEvents) eventos. Viva as melhores experiências por todo o Brasil."
        
        // Range of the message that needs to be bold and red
        let boldMessageRange = (message as NSString).range(of: "\(numberOfEvents) eventos.")
        
        // Attributed string of the initial message
        let attributedMessage = NSMutableAttributedString(string: message, attributes: [NSFontAttributeName: UIFont(name: "OpenSans", size: 20.0)!, NSForegroundColorAttributeName: UIColor.black])
        
        // Change tha range
        attributedMessage.setAttributes([NSFontAttributeName: UIFont(name: "OpenSans-Bold", size: 20.0)!, NSForegroundColorAttributeName: UIColor.red], range: boldMessageRange)
        
        // Set the attributed text of the label
        welcomeMessageLabel.attributedText = attributedMessage
        
    }
    
    /// Called when a like button from a featured event is pressed.
    ///
    /// First get the event that is associated to the button and then
    /// proceeds with that "like" action.
    func likeButtonFromFeaturedEventPressed(sender: UIButton) {
        
        // The button tag stores the event index
        let index = sender.tag
        
        // Check for error
        guard index < featuredEvents.count else {
            return
        }
        
        let event = featuredEvents[index]
        
        print("Like button from featured event \"\(event.name)\" pressed")
        
        likeEvent(event)
        
    }
    
    /// Called when a like button from a highlighted event is pressed.
    ///
    /// First get the event that is associated to the button and then
    /// proceeds with that "like" action.
    func likeButtonFromHighlightedEventPressed(sender: UIButton) {
        
        // The button tag stores the event index
        let index = sender.tag
        
        // Check for error
        guard index < highlightedEvents.count else {
            return
        }
        
        let event = highlightedEvents[index]
        
        print("Like button from highlighted event \"\(event.name)\" pressed")
        
        likeEvent(event)
    }
    
    /// This function will make the "like" action.
    func likeEvent(_ event: Event) {
        // TODO
    }
}


// MARK: Collection View Data Source
extension MainViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == featuredEventsCollectionView {
            return featuredEvents.count
        } else {
            return highlightedEvents.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == featuredEventsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCell", for: indexPath) as! FeaturedEventCollectionViewCell
            
            cell.configure(withEvent: featuredEvents[indexPath.row], index: indexPath.row)
            
            // Add the new target for the like button
            cell.likeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(self.likeButtonFromFeaturedEventPressed(sender:)), for: .touchUpInside)
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightedCell", for: indexPath) as! HighlightedEventCollectionViewCell
            
            cell.configure(withEvent: highlightedEvents[indexPath.row], index: indexPath.row)
            
            // Add the new target for the like button
            cell.likeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(self.likeButtonFromHighlightedEventPressed(sender:)), for: .touchUpInside)
            
            return cell
        }
    }
}

// MARK: Collection View Delegate
extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        // Get the selected event and open its web page
        if collectionView == featuredEventsCollectionView {
            let event = featuredEvents[indexPath.row]
            UIApplication.shared.open(event.url)
        } else {
            let event = highlightedEvents[indexPath.row]
            UIApplication.shared.open(event.url)
        }
    }
    
}

