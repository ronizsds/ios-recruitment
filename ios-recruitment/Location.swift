//
//  Location.swift
//  ios-recruitment
//
//  Created by Roniel Soares De Sousa on 21/04/17.
//  Copyright © 2017 Roniel Soares De Sousa. All rights reserved.
//

import Foundation

struct Location {
    let name: String
    let address: String
    let addressNum: String
    let addressAlt: String
    let neighborhood: String
    let city: String
    let state: String
    let stateDesc: String
    let zipCode: String
}


// MARK: Location Initializer

/// Add an optional initializer from JSON to Location
extension Location {
    
    /// Returns nil if deserialization fails
    init?(json: [String: Any]) {
        
        // Try to extract all values
        guard let name = json["name"] as? String,
            let address = json["address"] as? String,
            let addressNum = json["address_num"] as? String,
            let addressAlt = json["address_alt"] as? String,
            let neighborhood = json["neighborhood"] as? String,
            let city = json["city"] as? String,
            let state = json["state"] as? String,
            let stateDesc = json["state_desc"] as? String,
            let zipCode = json["zip_code"] as? String
        else {
            return nil
        }
        
        // Initialize properties
        self.name = name
        self.address = address
        self.addressNum = addressNum
        self.addressAlt = addressAlt
        self.neighborhood = neighborhood
        self.city = city
        self.state = state
        self.stateDesc = stateDesc
        self.zipCode = zipCode
    }
}
